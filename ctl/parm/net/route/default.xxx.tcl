#ENV
net route /SOC/default {
    description "Default-SOC - Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    gw _defaultgw.xxx.socialsecurity.be_%636
    mtu 0
    network default%636
    partition SOC
}
