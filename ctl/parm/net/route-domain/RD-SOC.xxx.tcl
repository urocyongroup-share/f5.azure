#ENV
net route-domain /SOC/RD-SOCSEC {
    app-service none
    bwc-policy none
    connection-limit 0
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    flow-eviction-policy none
    id 636
    ip-intelligence-policy none
    parent none
    partition SOC
    routing-protocol none
    service-policy none
    strict enabled
    vlans {
        /SOC/f5-itc-socsec
    }
}
