#ENV prd acc int tst
ltm snatpool /Common/wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com {
    app-service none
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    members {
        _wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com_
    }
    partition Common
}
