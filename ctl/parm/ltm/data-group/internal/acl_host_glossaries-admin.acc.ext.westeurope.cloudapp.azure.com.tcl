#ENV ALL
ltm data-group internal /Common/acl_host_glossaries-admin.acc.ext.westeurope.cloudapp.azure.com {
    app-service none
    description "Map known Smals IP ranges to their use type"
    partition Common
    records {
		85.91.175.192/27 {
			data "proxy smals"
        }
    }
    type ip
}
