#ENV ALL
ltm data-group internal /Common/smalsAcl {
    app-service none
    description "Map known Smals IP ranges to their use type"
    partition Common
    records {
        10.0.175.198 {
            data "belinda13"
        }
        10.0.175.200 {
            data "belinda54"
        } 
        10.21.252.0/24 {
            data "monitoring"
        }
        78.21.248.157/32 {
            data "podmi"
        }
        81.82.251.44/32 {
            data "dimona Pirsol"
        }
        82.143.94.144/32 {
            data "dimona group S"
        }
        84.199.237.176/28 {
            data "deloitte 1"
        }
        84.199.33.74/32 {
            data "dimona group S"
        }
        85.91.160.0/19 {
            data "Smals Official IPs"
        }
        85.91.162.1/32 {
            data "smals hide sc"
        }
        85.91.162.130/32 {
            data "proxysmals Blue Coat"
        }
        85.91.162.131/32 {
            data "proxysmals Blue Coat"
        }
        85.91.175.192/27 {
            data "proxy smals"
        }
        85.91.175.213/32 {
            data "proxyuser Blue Coat"
        }
        85.91.175.221/32 {
            data "proxyuser Blue Coat"
        }
        85.91.175.222/32 {
            data "proxyuser Blue Coat"
        }
        85.91.180.112 {
            data "smals WIFI"
        }
        85.91.187.65/32 {
            data "onss/rsz hide sc"
        }
        85.91.187.66/32 {
            data "onss/rsz hide sc"
        }
        85.91.191.4/32 {
            data "podmi hide sc"
        }
        100.64.85.128/26 {
            data "Gcloud services PAAS PRD"
        }
        185.35.52.191/32 {
            data "ebox bosa Centran Cloud"
        }
        185.153.68.0/22 {
            data "SSB"
        }
        185.180.76.0/22 {
            data "EC3 ATOS"
        }
        82.143.116.242/32 {
            data "EC3 CGSLB"
        }
        193.110.250.229/32 {
            data "EC3 CGSLB"
        }
        193.109.234.241/32 {
            data "dimona SD Works"
        }
        193.191.192.0/18 {
            data "FEDMAN"
        }
        193.191.212.55/32 {
            data "podmi"
        }
        193.191.245.205/32 {
            data "ebox bosa Outgoing Greenshift"
        }
        193.191.245.241/32 {
            data "ebox bosa"
        }
        194.78.42.144/32 {
            data "dimona group S"
        }
        194.78.54.214/32 {
            data "dimona UCM"
        }
        194.78.136.234 {
            data "deloitte 2"
        }
        195.0.32.0/24 {
            data "dimona acerta"
        }
        195.85.246.44/32 {
            data "dimona SD Works"
        }
        212.123.11.26/32 {
            data "dimona partena standby"
        }
        212.123.11.27/32 {
            data "dimona partena active"
        }
        10.0.142.0/23 {
            data "socsec INT vmaas for apiportal"
        }
        2a01:690:7:100::2:afd5/128 {
            data "proxyuser Blue Coat"
        }
        2a01:690:7:100::2:afdd/128 {
            data "proxyuser Blue Coat"
        }
        2a01:690:7:100::2:afde/128 {
            data "proxyuser Blue Coat"
        }
    }
    type ip
}
