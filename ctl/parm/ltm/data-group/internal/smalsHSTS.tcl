#ENV ALL
ltm data-group internal /Common/smalsHSTS {
    app-service none
    description "List with domains where we enable HSTS"
    partition Common
    records {
        "bijklussen.be" {
            data "max-age=60; includeSubDomains"
        }
        "activitescomplementaires.be" {
            data "max-age=60; includeSubDomains"
        }
        "verenigingswerk.be" {
            data "max-age=60; includeSubDomains"
        }
        "travailassociatif.be" {
            data "max-age=60; includeSubDomains"
        }
        "vereinsarbeit.be" {
            data "max-age=60; includeSubDomains"
        }
        "nebenerwerb.be" {
            data "max-age=60; includeSubDomains"
        }
        "www.bijklussen.be" {
            data "max-age=60; includeSubDomains"
        }
        "www.activitescomplementaires.be" {
            data "max-age=60; includeSubDomains"
        }
        "www.verenigingswerk.be" {
            data "max-age=60; includeSubDomains"
        }
        "www.travailassociatif.be" {
            data "max-age=60; includeSubDomains"
        }
        "www.vereinsarbeit.be" {
            data "max-age=60; includeSubDomains"
        }
        "www.nebenerwerb.be" {
            data "max-age=60; includeSubDomains"
        }
        "wwwacc.bijklussen.be" {
            data "max-age=60; includeSubDomains"
        }
        "wwwacc.activitescomplementaires.be" {
            data "max-age=60; includeSubDomains"
        }
        "wwwacc.verenigingswerk.be" {
            data "max-age=60; includeSubDomains"
        }
        "wwwacc.travailassociatif.be" {
            data "max-age=60; includeSubDomains"
        }
        "wwwacc.vereinsarbeit.be" {
            data "max-age=60; includeSubDomains"
        }
        "wwwwacc.nebenerwerb.be" {
            data "max-age=60; includeSubDomains"
        }
    }
    type string
}
