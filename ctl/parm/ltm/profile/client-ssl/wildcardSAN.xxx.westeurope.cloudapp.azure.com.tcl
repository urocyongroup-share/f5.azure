#ENV ALL
ltm profile client-ssl /Common/wildcardSAN.xxx.westeurope.cloudapp.azure.com {
    alert-timeout indefinite
    allow-dynamic-record-sizing disabled
    allow-expired-crl disabled
    allow-non-ssl disabled
    app-service none
    authenticate once
    authenticate-depth 9
    #ca-file socCABundle.crt
    cache-size 262144
    cache-timeout 3600
    cert-extension-includes { basic-constraints subject-alternative-name }
    cert-key-chain {
        wildcardSAN.xxx.westeurope.cloudapp.azure.com {
            app-service none
            cert /Common/wildcardSAN.xxx.westeurope.cloudapp.azure.com.crt
            chain /Common/QuoVadisEuropeSSLCAG1.crt
            key /Common/wildcardSAN.xxx.westeurope.cloudapp.azure.com.key
            #ocsp-stapling-params GlobalSignOCSP
            passphrase [vault][f5/f5.azure/ltm/profile/client-ssl/wildcardSAN.xxx.westeurope.cloudapp.azure.com.tcl][passphrase]
        }
    }
    cert-lifespan 30
    cert-lookup-by-ipaddr-port disabled
    ciphers !TLSv1:!TLSv1_1:!EXPORT:!DHE+AES-GCM:!DHE+AES:!DHE+3DES:!ECDHE+3DES:!RSA+3DES:ECDHE+AES-GCM:ECDHE+AES:RSA+AES-GCM:RSA+AES:-MD5:-SSLv3:-RC4
    #client-cert-ca socCABundle.crt
    crl-file none
    defaults-from none
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    destination-ip-blacklist none
    destination-ip-whitelist none
    forward-proxy-bypass-default-action intercept
    generic-alert enabled
    handshake-timeout 10
    hostname-blacklist none
    hostname-whitelist none
    inherit-certkeychain false
    max-active-handshakes indefinite
    max-aggregate-renegotiation-per-minute indefinite
    max-renegotiations-per-minute 5
    maximum-record-size 16384
    mod-ssl-methods disabled
    mode enabled
    options { dont-insert-empty-fragments }
    partition Common
    #passphrase $M$JO$TWDUfr7D4VHcuPqYccPb0Q==
    peer-cert-mode ignore
    peer-no-renegotiate-timeout 10
    proxy-ca-cert none
    proxy-ca-key none
    proxy-ca-passphrase none
    proxy-ssl disabled
    proxy-ssl-passthrough disabled
    renegotiate-max-record-delay indefinite
    renegotiate-period indefinite
    renegotiate-size indefinite
    renegotiation enabled
    retain-certificate true
    secure-renegotiation require
    server-name none
    session-mirroring disabled
    session-ticket disabled
    session-ticket-timeout 0
    sni-default true
    sni-require false
    source-ip-blacklist none
    source-ip-whitelist none
    ssl-forward-proxy disabled
    ssl-forward-proxy-bypass disabled
    ssl-sign-hash any
    strict-resume disabled
    unclean-shutdown enabled
}
