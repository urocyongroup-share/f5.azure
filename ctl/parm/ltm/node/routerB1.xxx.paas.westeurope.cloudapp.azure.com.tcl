#ENV prd acc int tst
ltm node /Common/routerB1.xxx.paas.westeurope.cloudapp.azure.com {
    address _routerB1.xxx.paas.westeurope.cloudapp.azure.com_
    app-service none
    connection-limit 0
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    dynamic-ratio 1
    fqdn {
        address-family ipv4
        autopopulate disabled
        down-interval 5
        interval 3600
        name none
    }
    logging disabled
    metadata none
    monitor default
    partition Common
    rate-limit disabled
    ratio 1
    session monitor-enabled
    state up
}
