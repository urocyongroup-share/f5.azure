#ENV ALL
ltm rule /Common/wildcardVip.xxx.ext.westeurope.cloudapp.azure.com_80 {
# Managed by Git repo f5.azure
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when HTTP_REQUEST priority 500 {

  # Check which host the client requested
  switch -- $httpHost \
    default {
      # Redirect all other requests to https
      HTTP::redirect "https://$httpHost$httpUri"
      # log request
      call smalsProcs::accesslog_v1_302 "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName"
    }
}

app-service none
partition Common
}
