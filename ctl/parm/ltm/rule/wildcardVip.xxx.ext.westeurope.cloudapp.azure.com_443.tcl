#ENV ALL
ltm rule /Common/wildcardVip.xxx.ext.westeurope.cloudapp.azure.com_443 {
# Managed by Git repo f5.azure
# Version: $Id$
# $Format:Git ID: (%h) %ci/%cn$

when HTTP_REQUEST priority 500 {

  # Check which host the client requested
  switch -glob -- $httpHost \
    "tesos.ext.westeurope.cloudapp.azure.com" - \
    "tesos-admin.ext.westeurope.cloudapp.azure.com" - \
    "falco.ext.westeurope.cloudapp.azure.com" - \
    "falco-admin.ext.westeurope.cloudapp.azure.com" - \
    "phoenix.westeurope.cloudapp.azure.com" - \
    "phoenix-acpt.westeurope.cloudapp.azure.com" - \
    "phoenix-xxx.westeurope.cloudapp.azure.com" - \
    "*.test.ext.westeurope.cloudapp.azure.com" - \
    "reuse.smals.be" - \
    "reuse.xxx.smals.be" - \
    "*.xxx.ext.westeurope.cloudapp.azure.com" {
      pool router.xxx.paas.westeurope.cloudapp.azure.com_80
    } \
    default {
      # Block all other requests
      HTTP::respond 403
      call smalsProcs::accesslog_v1_403 "$ipClient" "$httpHost" "$httpRequestTime" "$httpMethod" "$httpUri" "$httpVersion" "$httpReferer" "$httpUserAgent" "$sslClientCipherNameLog" "$sslClientCipherVersionLog" "$sslClientCipherBitsLog" "$vsName" "$smalsEnv" "$partitionName"
    }
}

app-service none
partition Common
}
