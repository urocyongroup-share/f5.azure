#ENV prd acc int tst
ltm virtual-address /Common/wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com {
    address _wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com_
    app-service none
    arp enabled
    auto-delete true
    connection-limit 0
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    enabled yes
    floating enabled
    icmp-echo enabled
    inherited-traffic-group false
    mask 255.255.255.255
    metadata none
    partition Common
    route-advertisement disabled
    server-scope any
    spanning disabled
}
