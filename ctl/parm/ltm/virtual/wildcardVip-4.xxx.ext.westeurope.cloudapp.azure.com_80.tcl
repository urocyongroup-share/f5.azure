#ENV prd acc int tst
ltm virtual /Common/wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com_80 {
    address-status yes
    app-service none
    auth none
    auto-lasthop default
    bwc-policy none
    cmp-enabled yes
    connection-limit 0
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    destination wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com:80
    enabled
    fallback-persistence none
    flow-eviction-policy none
    gtm-score 0
    ip-protocol tcp
    last-hop-pool none
    mask 255.255.255.255
    metadata none
    mirror disabled
    mobile-app-tunnel disabled
    nat64 disabled
    partition Common
    per-flow-request-access-policy none
    persist {
        smalsCookie {
            default yes
        }
    }
    policies none
    #policies {
        #/Common/ltm_xxx_asm_default { }
    #}
    pool none
    profiles {
        http {
            context all
        }
        smalsTcp {
            context all
        }
        websecurity {
            context all
        }
    }
    rate-class none
    rate-limit disabled
    rate-limit-dst-mask 0
    rate-limit-mode object
    rate-limit-src-mask 0
    related-rules none
    rules {
        smalsEnv.xxx
        smalsIp
        smalsTcp
        smalsLb
        smalsHttp
        smalsHttpAcl
        wildcardVip.xxx.ext.westeurope.cloudapp.azure.com_80
    }
    security-log-profiles none
    service-down-immediate-action none
    service-policy none
    source 0.0.0.0/0
    source-address-translation {
        pool wildcardVip-4.xxx.ext.westeurope.cloudapp.azure.com
        type snat
    }
    source-port preserve
    syn-cookie-status not-activated
    traffic-classes none
    translate-address enabled
    translate-port enabled
    urldb-feed-policy none
    vlans none
    vlans-disabled
}
