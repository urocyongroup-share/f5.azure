#ENV prd acc int tst
ltm pool /Common/router.xxx.paas.westeurope.cloudapp.azure.com_80 {
    allow-nat yes
    allow-snat yes
    app-service none
    autoscale-group-id none
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    gateway-failsafe-device none
    ignore-persisted-weight disabled
    ip-tos-to-client pass-through
    ip-tos-to-server pass-through
    link-qos-to-client pass-through
    link-qos-to-server pass-through
    load-balancing-mode round-robin
    members {
        routerA1.xxx.paas.westeurope.cloudapp.azure.com:80 {
            address _routerA1.xxx.paas.westeurope.cloudapp.azure.com_
            app-service none
            connection-limit 0
            description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
            dynamic-ratio 1
            inherit-profile enabled
            logging disabled
            monitor default
            priority-group 0
            rate-limit disabled
            ratio 1
            session monitor-enabled
            state checking
            fqdn {
                autopopulate disabled
                name none
            }
            metadata none
            profiles none
        }
        routerB1.xxx.paas.westeurope.cloudapp.azure.com:80 {
            address _routerB1.xxx.paas.westeurope.cloudapp.azure.com_
            app-service none
            connection-limit 0
            description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
            dynamic-ratio 1
            inherit-profile enabled
            logging disabled
            monitor default
            priority-group 0
            rate-limit disabled
            ratio 1
            session monitor-enabled
            state checking
            fqdn {
                autopopulate disabled
                name none
            }
            metadata none
            profiles none
        }
    }
    metadata none
    min-active-members 0
    min-up-members 0
    min-up-members-action failover
    min-up-members-checking disabled
    monitor fastTcp
    partition Common
    profiles none
    queue-depth-limit 0
    queue-on-connection-limit disabled
    queue-time-limit 0
    reselect-tries 0
    service-down-action reselect
    slow-ramp-time 10
}
