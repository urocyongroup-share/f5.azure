#ENV sim acc int tst dev
security dos profile /Common/dos_profile_azure_xxx_generic {
    app-service none
    application {
        dos_profile_soc_acc_generic {
            app-service none
            fastl4-acceleration-profile none
            single-page-application disabled
            trigger-irule disabled
            url-patterns none
            bot-defense {
                browser-legit-captcha disabled
                browser-legit-enabled enabled
                cross-domain-requests allow-all
                external-domains none
                grace-period 300
                mode disabled
                site-domains none
                url-whitelist none
            }
            bot-signatures {
                categories {
                    "DOS Tool" {
                        action report
                        app-service none
                    }
                    "E-Mail Collector" {
                        action report
                        app-service none
                    }
                    "Exploit Tool" {
                        action report
                        app-service none
                    }
                    "HTTP Library" {
                        action report
                        app-service none
                    }
                    "Network Scanner" {
                        action report
                        app-service none
                    }
                    "Search Bot" {
                        action report
                        app-service none
                    }
                    "Search Engine" {
                        action report
                        app-service none
                    }
                    "Service Agent" {
                        action report
                        app-service none
                    }
                    "Site Monitor" {
                        action report
                        app-service none
                    }
                    "Social Media Agent" {
                        action report
                        app-service none
                    }
                    "Spam Bot" {
                        action report
                        app-service none
                    }
                    "Vulnerability Scanner" {
                        action report
                        app-service none
                    }
                    "Web Downloader" {
                        action report
                        app-service none
                    }
                    "Web Spider" {
                        action report
                        app-service none
                    }
                    Crawler {
                        action report
                        app-service none
                    }
                }
                check enabled
                disabled-signatures none
            }
            captcha-response {
                failure {
                    body "You have entered an invalid answer for the question. Please, try again.
<br>
%DOSL7.captcha.image% %DOSL7.captcha.change%
<br>
<b>What code is in the image\?</b>
%DOSL7.captcha.solution%
<br>
%DOSL7.captcha.submit%
<br>
<br>
Your support ID is: %DOSL7.captcha.support_id%."
                    type default
                }
                first {
                    body "This question is for testing whether you are a human visitor and to prevent automated spam submission.
<br>
%DOSL7.captcha.image% %DOSL7.captcha.change%
<br>
<b>What code is in the image\?</b>
%DOSL7.captcha.solution%
<br>
%DOSL7.captcha.submit%
<br>
<br>
Your support ID is: %DOSL7.captcha.support_id%."
                    type default
                }
            }
            geolocations none
            heavy-urls {
                automatic-detection enabled
                exclude none
                include none
                include-list none
                latency-threshold 1000
                protection enabled
            }
            ip-whitelist none
            mobile-detection {
                allow-android-rooted-device false
                allow-any-android-package false
                allow-any-ios-package false
                allow-emulators false
                allow-jailbroken-devices false
                android-publishers none
                client-side-challenge-mode pass
                enabled disabled
                ios-allowed-package-names none
            }
            stress-based {
                behavioral {
                    dos-detection disabled
                    mitigation-mode none
                    signatures disabled
                    signatures-approved-only disabled
                }
                de-escalation-period 7200
                device-captcha-challenge disabled
                device-client-side-defense disabled
                device-maximum-auto-tps 5000
                device-maximum-tps 200
                device-minimum-auto-tps 5
                device-minimum-tps 40
                device-rate-limiting disabled
                device-request-blocking-mode rate-limit
                device-tps-increase-rate 500
                escalation-period 120
                geo-captcha-challenge disabled
                geo-client-side-defense disabled
                geo-maximum-auto-tps 20000
                geo-minimum-auto-tps 50
                geo-minimum-share 10
                geo-rate-limiting disabled
                geo-request-blocking-mode rate-limit
                geo-share-increase-rate 500
                ip-captcha-challenge disabled
                ip-client-side-defense disabled
                ip-maximum-auto-tps 5000
                ip-maximum-tps 200
                ip-minimum-auto-tps 5
                ip-minimum-tps 40
                ip-rate-limiting enabled
                ip-request-blocking-mode rate-limit
                ip-tps-increase-rate 500
                mode transparent
                site-captcha-challenge disabled
                site-client-side-defense disabled
                site-maximum-auto-tps 20000
                site-maximum-tps 10000
                site-minimum-auto-tps 50
                site-minimum-tps 2000
                site-rate-limiting disabled
                site-tps-increase-rate 500
                thresholds-mode manual
                url-captcha-challenge disabled
                url-client-side-defense disabled
                url-enable-heavy enabled
                url-maximum-auto-tps 5000
                url-maximum-tps 1000
                url-minimum-auto-tps 50
                url-minimum-tps 200
                url-rate-limiting enabled
                url-tps-increase-rate 500
            }
            tcp-dump {
                maximum-duration 30
                maximum-size 10
                record-traffic disabled
                repetition-interval 120
            }
            tps-based {
                de-escalation-period 7200
                device-captcha-challenge disabled
                device-client-side-defense disabled
                device-maximum-auto-tps 5000
                device-maximum-tps 200
                device-minimum-auto-tps 5
                device-minimum-tps 40
                device-rate-limiting disabled
                device-request-blocking-mode rate-limit
                device-tps-increase-rate 500
                escalation-period 120
                geo-captcha-challenge disabled
                geo-client-side-defense disabled
                geo-maximum-auto-tps 20000
                geo-minimum-auto-tps 50
                geo-minimum-share 10
                geo-rate-limiting disabled
                geo-request-blocking-mode rate-limit
                geo-share-increase-rate 500
                ip-captcha-challenge disabled
                ip-client-side-defense disabled
                ip-maximum-auto-tps 5000
                ip-maximum-tps 200
                ip-minimum-auto-tps 5
                ip-minimum-tps 40
                ip-rate-limiting enabled
                ip-request-blocking-mode rate-limit
                ip-tps-increase-rate 500
                mode transparent
                site-captcha-challenge disabled
                site-client-side-defense disabled
                site-maximum-auto-tps 20000
                site-maximum-tps 10000
                site-minimum-auto-tps 50
                site-minimum-tps 2000
                site-rate-limiting disabled
                site-tps-increase-rate 500
                thresholds-mode manual
                url-captcha-challenge disabled
                url-client-side-defense disabled
                url-enable-heavy enabled
                url-maximum-auto-tps 5000
                url-maximum-tps 1000
                url-minimum-auto-tps 50
                url-minimum-tps 200
                url-rate-limiting enabled
                url-tps-increase-rate 500
            }
        }
    }
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    http-whitelist none
    partition SOC
    threshold-sensitivity medium
    whitelist none
}
