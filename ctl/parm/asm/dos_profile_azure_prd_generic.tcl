#ENV prd
security dos profile /Common/dos_profile_azure_prd_generic {
    app-service none
    application none
    description "Managed by Git repo f5.azure - Version: $Id$ - $Format:Git ID: (%h) %ci - %cn(%ce)$"
    partition SOC
    whitelist none
}
